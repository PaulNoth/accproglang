package com.accenture.lang;

import com.accenture.lang.domain.Instruction;

import java.util.ArrayList;
import java.util.List;

public class Program {
    private List<Instruction> instructions;
    private String className;

    public Program(String className, List<Instruction> instructions) {
        this.className = className;
        this.instructions = new ArrayList<>(instructions);
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public String getClassName() {
        return className;
    }
}
