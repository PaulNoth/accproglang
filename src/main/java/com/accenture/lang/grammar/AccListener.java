package com.accenture.lang.grammar;// Generated from Acc.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link AccParser}.
 */
public interface AccListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link AccParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(AccParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link AccParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(AccParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link AccParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(AccParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link AccParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(AccParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link AccParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(AccParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link AccParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(AccParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link AccParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(AccParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link AccParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(AccParser.ValueContext ctx);
}