package com.accenture.lang;

import com.accenture.lang.generator.ByteCodeGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Compiler {
    public static void main(String[] args) throws Exception {
        new Compiler().compile(args);
    }

    public void compile(String[] args) throws Exception{
        File file = new File(args[0]);
        Program compilationUnit = new ProgramUnitParser().getProgramCompilationUnit(file);

        saveBytecodeToClassFile(compilationUnit);
    }

    private static void saveBytecodeToClassFile(Program compilationUnit) throws IOException {
        String className = compilationUnit.getClassName();
        byte[] byteCode = new ByteCodeGenerator().generateByteCode(compilationUnit);
        final String classFile = className + ".class";
        OutputStream os = new FileOutputStream(classFile);
        os.write(byteCode);
        os.close();
    }
}
