package com.accenture.lang.domain;

import org.objectweb.asm.MethodVisitor;

public interface Instruction {
    void apply(MethodVisitor mv);
}
