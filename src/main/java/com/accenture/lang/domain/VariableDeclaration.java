package com.accenture.lang.domain;

import com.accenture.lang.grammar.AccLexer;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class VariableDeclaration implements Instruction {
    private Variable variable;

    public VariableDeclaration(Variable variable) {
        this.variable = variable;
    }

    @Override
    public void apply(MethodVisitor mv) {
        final int type = variable.getType();
        if(type == AccLexer.NUMBER) {
            int val = Integer.valueOf(variable.getValue());
            mv.visitIntInsn(Opcodes.BIPUSH, val);
            mv.visitVarInsn(Opcodes.ISTORE, variable.getId());
        } else if(type == AccLexer.STRING) {
            mv.visitLdcInsn(variable.getValue());
            mv.visitVarInsn(Opcodes.ASTORE, variable.getId());
        }
    }
}
