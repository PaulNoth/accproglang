package com.accenture.lang.domain;

public class Variable {
    private final int variableIndex;
    private final int variableType;
    private final String stringValue;

    public Variable(int variableIndex, int varType, String value) {
        this.variableIndex = variableIndex;
        this.variableType = varType;
        this.stringValue = value;
    }

    public int getId() {
        return variableIndex;
    }

    public int getType() {
        return variableType;
    }

    public String getValue() {
        return stringValue;
    }
}
