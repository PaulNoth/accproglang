package com.accenture.lang.generator;

import com.accenture.lang.Program;
import com.accenture.lang.domain.Instruction;
import com.accenture.lang.domain.VariableDeclaration;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.List;

public class ByteCodeGenerator {
    private static int BYTE_CODE_VERSION = 52;

    public byte[] generateByteCode(Program compilationUnit) {
        ClassWriter cw = new ClassWriter(0);
        MethodVisitor mw;

        List<Instruction> instructions = compilationUnit.getInstructions();
        String className = compilationUnit.getClassName();
        // version, access, name, signature, base class, interfaces
        cw.visit(BYTE_CODE_VERSION, Opcodes.ACC_PUBLIC + Opcodes.ACC_SUPER, className, null, "java/lang/Object", null);

        // declare static void main
        mw = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
        long localVariablesCount = instructions.stream()
                .filter(instruction -> instruction instanceof VariableDeclaration).count();
        int maxStack = 100; //
        for(Instruction ins : instructions) {
            ins.apply(mw);
        }
        mw.visitInsn(Opcodes.RETURN);
        mw.visitMaxs(maxStack, (int) localVariablesCount);
        mw.visitEnd();

        cw.visitEnd();
        return cw.toByteArray();
    }
}
