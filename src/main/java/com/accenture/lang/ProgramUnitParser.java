package com.accenture.lang;

import com.accenture.lang.domain.Instruction;
import com.accenture.lang.listener.SyntaxTreeTraverser;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ProgramUnitParser {
    public Program getProgramCompilationUnit(File file) throws IOException {
        String fileName = file.getName();
        String fileAbsPath = file.getAbsolutePath();
        String className = fileName.substring(0, fileName.lastIndexOf('.'));

        List<Instruction> instructions = new SyntaxTreeTraverser().getInstructions(fileAbsPath);
        return new Program(className, instructions);
    }
}
