package com.accenture.lang.listener;

import com.accenture.lang.domain.Instruction;
import com.accenture.lang.domain.PrintVariable;
import com.accenture.lang.domain.Variable;
import com.accenture.lang.domain.VariableDeclaration;
import com.accenture.lang.grammar.AccBaseListener;
import com.accenture.lang.grammar.AccParser;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccTreeWalkListener extends AccBaseListener {
    private List<Instruction> instructions;
    private Map<String, Variable> variables = new HashMap<>();

    public AccTreeWalkListener() {
        instructions = new ArrayList<>();
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    @Override
    public void exitPrint(AccParser.PrintContext ctx) {
        TerminalNode varName = ctx.ID();
        boolean printVarNotDeclared = !variables.containsKey(varName.getText());
        if(printVarNotDeclared) {
            String err = "You are trying to print var '%s' which has not been declared";
            System.out.printf(err, varName.getText());
        }
        Variable var = variables.get(varName.getText());
        instructions.add(new PrintVariable(var));
    }

    @Override
    public void exitVariable(AccParser.VariableContext ctx) {
        TerminalNode varName = ctx.ID();
        AccParser.ValueContext varValue = ctx.value();
        int varType = varValue.getStart().getType();
        int varIndex = variables.size();
        String varTextValue = varValue.getText();
        Variable var = new Variable(varIndex, varType, varTextValue);
        variables.put(varName.getText(), var);
        instructions.add(new VariableDeclaration(var));
    }
}
