package com.accenture.lang.listener;

import com.accenture.lang.domain.Instruction;
import com.accenture.lang.grammar.AccLexer;
import com.accenture.lang.grammar.AccParser;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;
import java.util.List;

public class SyntaxTreeTraverser {
    public List<Instruction> getInstructions(String fileAbsolutePath) throws IOException {
        CharStream charStream = new ANTLRFileStream(fileAbsolutePath);
        AccLexer accLexer = new AccLexer(charStream);
        CommonTokenStream commonTokenStream  = new CommonTokenStream(accLexer);
        AccParser accParser = new AccParser(commonTokenStream);
        AccTreeWalkListener accTreeWalkListener = new AccTreeWalkListener();

        //BaseErrorListener errorListener = new EnkelTreeWalkErrorListener();
        accParser.addParseListener(accTreeWalkListener);
        //accParser.addErrorListener(errorListener);
        accParser.program();
        return accTreeWalkListener.getInstructions();
    }
}
